package exeptions;

public class NumberNotAllowedException extends RuntimeException {

    public NumberNotAllowedException(String message) {
        super(message);
    }
}
