package org.example;

import exeptions.NumberNotAllowedException;
public class StringCalculator {
    public static int calculate(String s) {
        int sum = 0;
        String sep = ",";
        if (s.isEmpty() || s.isBlank()) {
            return 0;
        }
        else if (!Character.isDigit(s.charAt(0))) {
            sep = String.valueOf(s.charAt(0));
            s = s.substring(1);
        }
        for (String ch : s.split("["+ sep +"]")) {
            try{
                int num = Integer.parseInt(ch);
                if (num >= 0 && num <= 1000)
                    sum += num;
                else if (num < 0)
                    throw new NumberNotAllowedException("negative numbers are not allowed");
            } catch (NumberFormatException e) {
                throw new NumberNotAllowedException("only numbers are allowed");
            }
        }
        return sum;
    }
}
