package org.example;

import exeptions.NumberNotAllowedException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StringCalculatorTest {
    String str = "";
    @Test
    public void emptyString(){
        assertEquals(0, StringCalculator.calculate(str));
    }
    @Test
    public void stringWithOneNumber(){
        str = "2";
        assertEquals(Integer.valueOf(str), StringCalculator.calculate(str));
    }
    @Test
    public void commaSeparatedString(){
        str = "2,4,4";
        assertEquals(10, StringCalculator.calculate(str));
    }

    @Test
    public void negativeNumbersString(){
        str = "2,-4,4";
        assertThrows(NumberNotAllowedException.class, () -> StringCalculator.calculate(str));
    }

    @Test
    public void numberBiggerThanThousand(){
        str = "100,200,500,1100";
        assertEquals(800, StringCalculator.calculate(str));
    }

    @Test
    public void anOtherSeparator(){
        str = "/10/20/30";
        assertEquals(60, StringCalculator.calculate(str));
    }
}
