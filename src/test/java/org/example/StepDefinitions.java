package org.example;

import exeptions.NumberNotAllowedException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;


public class StepDefinitions {
    private String str;
    private int calculationResult;
    @Given("an empty string")
    public void an_empty_string() {
        str = "";
    }

    @When("the string is calculated")
    public void the_string_is_calculated(){
            calculationResult = StringCalculator.calculate(str);
    }

    @Then("the result is {int}")
    public void the_result_is(Integer result) {
        assertEquals(Optional.of(result), Optional.of(calculationResult));
    }

    @Given("a string {string}")
    public void a_string(String toBeCalculatedString) {
        str = toBeCalculatedString;
    }

    @Then("the calculation of the string throws a NegativeNumberNotAllowed exception")
    public void the_calculation_of_the_string_throws_a_negative_number_not_allowed_exception() {
        assertThrows(NumberNotAllowedException.class, () -> StringCalculator.calculate(str));
    }
}
